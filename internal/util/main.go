package util

import (
	"log"
	"os"
)

func ErrIsFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func OpenDay(day string) *os.File {
	input, err := os.Open("cmd/day" + day + "/input.txt")
	ErrIsFatal(err)
	return input
}
