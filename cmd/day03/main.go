package main

import (
	"bufio"
	"fmt"

	"github.com/kylesferrazza/adventofcode2020/internal/util"
)

type Slope struct {
	right  int
	down   int
	trees  int
	curCol int
}

func main() {
	input := util.OpenDay("03")
	defer input.Close()

	slopes := []Slope{
		{right: 1, down: 1},
		{right: 3, down: 1},
		{right: 5, down: 1},
		{right: 7, down: 1},
		{right: 1, down: 2},
	}

	scanner := bufio.NewScanner(input)
	rowNum := 0
	for scanner.Scan() {
		line := scanner.Text()
		for slopeNum := 0; slopeNum < len(slopes); slopeNum++ {
			slope := &slopes[slopeNum]
			if rowNum%slope.down != 0 {
				continue
			}
			if line[slope.curCol] == '#' {
				slope.trees++
			}
			slope.curCol = (slope.curCol + slope.right) % len(line)
		}
		rowNum++
	}

	fmt.Printf("PART 1: %d\n", slopes[1].trees)

	product := 1
	for slopeNum := 0; slopeNum < len(slopes); slopeNum++ {
		product *= slopes[slopeNum].trees
	}
	fmt.Printf("PART 2: %d\n", product)
}
