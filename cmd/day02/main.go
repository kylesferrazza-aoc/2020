package main

import (
	"bufio"
	"fmt"
	"log"
	"regexp"
	"strconv"

	"github.com/kylesferrazza/adventofcode2020/internal/util"
)

func isValidOne(from, to int, char byte, password string) bool {
	count := 0
	for _, c := range password {
		if c == rune(char) {
			count++
		}
	}
	return (count >= from) && (count <= to)
}

func isValidTwo(from, to int, char byte, password string) bool {
	return (password[from-1] == char) != (password[to-1] == char)
}

func main() {
	input := util.OpenDay("02")
	defer input.Close()

	re := regexp.MustCompile(`(\d+)-(\d+) ([a-z]): (.*)`)

	scanner := bufio.NewScanner(input)
	numValidOne := 0
	numValidTwo := 0
	for scanner.Scan() {
		line := scanner.Text()
		matches := re.FindStringSubmatch(line)
		if len(matches) < 5 {
			log.Fatalf("Badly formatted line: [%s]\n", line)
		}
		from, err := strconv.Atoi(matches[1])
		util.ErrIsFatal(err)
		to, err := strconv.Atoi(matches[2])
		util.ErrIsFatal(err)
		char := matches[3]
		if len(char) != 1 {
			log.Fatalf("Badly formatted char: [%s]\n", line)
		}
		pass := matches[4]
		if isValidOne(from, to, char[0], pass) {
			numValidOne++
		}
		if isValidTwo(from, to, char[0], pass) {
			numValidTwo++
		}
	}
	util.ErrIsFatal(scanner.Err())

	fmt.Printf("PART 1: %d\n", numValidOne)
	fmt.Printf("PART 2: %d\n", numValidTwo)
}
