package main

import (
	"bufio"
	"errors"
	"fmt"
	"strconv"

	"github.com/kylesferrazza/adventofcode2020/internal/util"
)

func findTwo2020(expenses []int) (a, b int, err error) {
	for _, x := range expenses {
		for _, y := range expenses {
			if x+y == 2020 {
				return x, y, nil
			}
		}
	}
	return 0, 0, errors.New("No 2020 sum found in expenses")
}

func findThree2020(expenses []int) (a, b, c int, err error) {
	for _, x := range expenses {
		for _, y := range expenses {
			for _, z := range expenses {
				if x+y+z == 2020 {
					return x, y, z, nil
				}
			}
		}
	}
	return 0, 0, 0, errors.New("No 2020 sum found in expenses")
}

func main() {
	input := util.OpenDay("01")
	defer input.Close()

	var expenses []int

	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		expense, err := strconv.Atoi(scanner.Text())
		util.ErrIsFatal(err)
		expenses = append(expenses, expense)
	}

	util.ErrIsFatal(scanner.Err())

	a, b, err := findTwo2020(expenses)
	util.ErrIsFatal(err)

	fmt.Printf("PART 1: %d\n", a*b)

	a, b, c, err := findThree2020(expenses)
	util.ErrIsFatal(err)

	fmt.Printf("PART 2: %d\n", a*b*c)
}
