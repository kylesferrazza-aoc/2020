package main

import (
	"bufio"
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/kylesferrazza/adventofcode2020/internal/util"
)

func inRange(test string, low, high int) bool {
	intVal, err := strconv.Atoi(test)
	if err != nil {
		return false
	}
	if intVal < low || intVal > high {
		return false
	}
	return true
}

func validPassportTwo(pass []string) bool {
	containsByr := false
	containsIyr := false
	containsEyr := false
	containsHgt := false
	containsHcl := false
	containsEcl := false
	containsPid := false
	for _, field := range pass {
		key := field[0:3]
		val := field[4:]
		switch key {
		case "byr":
			if len(val) != 4 {
				continue
			}
			if !inRange(val, 1920, 2002) {
				continue
			}
			containsByr = true
		case "iyr":
			if len(val) != 4 {
				continue
			}
			if !inRange(val, 2010, 2020) {
				continue
			}
			containsIyr = true
		case "eyr":
			if len(val) != 4 {
				continue
			}
			if !inRange(val, 2020, 2030) {
				continue
			}
			containsEyr = true
		case "hgt":
			if len(val) < 2 {
				continue
			}
			lastTwo := val[len(val)-2:]
			beginning := val[:len(val)-2]
			switch lastTwo {
			case "cm":
				if inRange(beginning, 150, 193) {
					containsHgt = true
				}
			case "in":
				if inRange(beginning, 59, 76) {
					containsHgt = true
				}
			}
		case "hcl":
			if len(val) != 7 {
				continue
			}
			if val[0] != '#' {
				continue
			}
			good := true
			for i := 1; i < 7; i++ {
				if !strings.ContainsRune("0123456789abcdef", rune(val[i])) {
					good = false
				}
			}
			if good {
				containsHcl = true
			}
		case "ecl":
			switch val {
			case "amb":
				containsEcl = true
			case "blu":
				containsEcl = true
			case "brn":
				containsEcl = true
			case "gry":
				containsEcl = true
			case "grn":
				containsEcl = true
			case "hzl":
				containsEcl = true
			case "oth":
				containsEcl = true
			}
		case "pid":
			if len(val) != 9 {
				continue
			}
			if _, err := strconv.Atoi(val); err != nil {
				continue
			}
			containsPid = true
		case "cid":
			// optional
		default:
			return false
		}
	}
	return containsByr && containsIyr && containsEyr && containsHgt && containsHcl && containsEcl && containsPid
}

func validPassportOne(pass []string) bool {
	containsByr := false
	containsIyr := false
	containsEyr := false
	containsHgt := false
	containsHcl := false
	containsEcl := false
	containsPid := false
	for _, field := range pass {
		switch field[0:3] {
		case "byr":
			containsByr = true
		case "iyr":
			containsIyr = true
		case "eyr":
			containsEyr = true
		case "hgt":
			containsHgt = true
		case "hcl":
			containsHcl = true
		case "ecl":
			containsEcl = true
		case "pid":
			containsPid = true
		case "cid":
			// optional
		default:
			return false
		}
	}
	return containsByr && containsIyr && containsEyr && containsHgt && containsHcl && containsEcl && containsPid
}

func main() {
	input := util.OpenDay("04")
	defer input.Close()

	scanner := bufio.NewScanner(input)
	curPassport := make([]string, 0, 8)
	numValidOne := 0
	numValidTwo := 0
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			if validPassportOne(curPassport) {
				numValidOne++
			}
			if validPassportTwo(curPassport) {
				numValidTwo++
			}
			curPassport = curPassport[:0]
			continue
		}
		fields := strings.Fields(line)
		curPassport = append(curPassport, fields...)
	}
	if validPassportOne(curPassport) {
		numValidOne++
	}
	if validPassportTwo(curPassport) {
		numValidTwo++
	}
	if err := scanner.Err(); err != nil {
		log.Fatalln(err)
	}

	fmt.Printf("PART 1: %d\n", numValidOne)
	fmt.Printf("PART 2: %d\n", numValidTwo)
}
