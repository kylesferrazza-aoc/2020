package main

import (
	"bufio"
	"fmt"
	"log"
	"sort"

	"github.com/kylesferrazza/adventofcode2020/internal/util"
)

func passToRowCol(pass string) (row, col int) {
	low := 0
	high := 127
	diff := 64
	for _, instr := range pass[:7] {
		switch instr {
		case 'F':
			high -= diff
		case 'B':
			low += diff
		default:
			log.Fatalf("Bad instruction: [%c]\n", instr)
		}
		diff /= 2
	}
	row = low

	low = 0
	high = 7
	diff = 4
	for _, instr := range pass[7:] {
		switch instr {
		case 'R':
			low += diff
		case 'L':
			high -= diff
		default:
			log.Fatalf("Bad instruction: [%c]\n", instr)
		}
		diff /= 2
	}
	col = low
	return
}

func rowColToSeatId(row, col int) (seatId int) {
	return row*8 + col
}

func main() {
	input := util.OpenDay("05")
	defer input.Close()

	seen := make([]int, 0)
	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		line := scanner.Text()
		row, col := passToRowCol(line)
		seatId := rowColToSeatId(row, col)
		seen = append(seen, seatId)
	}
	sort.Ints(seen)

	highestSeatId := seen[len(seen)-1]

	fmt.Printf("PART 1: %d\n", highestSeatId)

	missing := -1
	for i, id := range seen {
		if i < 1 {
			continue
		}
		gapWithPrev := (id - seen[i-1]) > 1
		if gapWithPrev {
			missing = id - 1
		}
	}

	fmt.Printf("PART 2: %d\n", missing)
}
