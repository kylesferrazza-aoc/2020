package main

import "testing"

var tests = []struct {
	pass     string
	row, col int
	seatId   int
}{
	{"BFFFBBFRRR", 70, 7, 567},
	{"FFFBBBFRRR", 14, 7, 119},
	{"BBFFBBFRLL", 102, 4, 820},
	{"BBFFBBFRLR", 102, 5, 821},
	{"BBFFBBBRLR", 103, 5, 829},
}

func TestRowCol(t *testing.T) {
	for _, test := range tests {
		if row, col := passToRowCol(test.pass); row != test.row || col != test.col {
			t.Errorf("[%s] failed: got (%d, %d), expected: (%d, %d)", test.pass, row, col, test.row, test.col)
		}
	}
}

func TestSeatID(t *testing.T) {
	for _, test := range tests {
		if seatId := rowColToSeatId(test.row, test.col); seatId != test.seatId {
			t.Errorf("(%d, %d) failed: got %d, expected %d", test.row, test.col, seatId, test.seatId)
		}
	}
}
