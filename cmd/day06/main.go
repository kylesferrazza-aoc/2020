package main

import (
	"bufio"
	"fmt"
	"strings"

	"github.com/kylesferrazza/adventofcode2020/internal/util"
)

func uniqueAlphas(str string) (ret int) {
	for i := 'a'; i <= 'z'; i++ {
		if strings.ContainsRune(str, i) {
			ret++
		}
	}
	return
}

func partOne() {
	input := util.OpenDay("06")
	defer input.Close()

	scanner := bufio.NewScanner(input)
	sum := 0
	var thisGroup string
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			sum += uniqueAlphas(thisGroup)
			thisGroup = ""
		} else {
			thisGroup += line
		}
	}
	sum += uniqueAlphas(thisGroup)

	fmt.Printf("PART 1: %d\n", sum)
}

// Given the size of a group in people and the count of each letter,
// return the number of letters with counts equal to the size of the group
func uniqueCount(size int, counts [26]int) (ret int) {
	for _, count := range counts {
		if count == size {
			ret++
		}
	}
	return
}

func partTwo() {
	input := util.OpenDay("06")
	defer input.Close()

	// Sum of all groups' counts
	sum := 0

	// Size (in people) of the current group
	size := 0

	// Count of each letter for the current group
	var counts [26]int

	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			// New group.
			sum += uniqueCount(size, counts)
			size = 0
			for i := range counts {
				counts[i] = 0
			}
		} else {
			size++
			for _, r := range line {
				ind := r - 'a'
				counts[ind]++
			}
		}
	}
	sum += uniqueCount(size, counts)

	fmt.Printf("PART 2: %d\n", sum)
}

func main() {
	partOne()
	partTwo()
}
