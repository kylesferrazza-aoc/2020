package main

import (
	"bufio"
	"fmt"

	"github.com/kylesferrazza/adventofcode2020/internal/util"
)

type BagContent struct {
	quantity int
	color    string
}

type Bag struct {
	color    string
	contents []BagContent
}

func parseBag(sentence string) (bag Bag) {
	return
}

func partOne() {
	input := util.OpenDay("07")
	defer input.Close()

	scanner := bufio.NewScanner(input)
	for scanner.Scan() {
		line := scanner.Text()
		fmt.Println(line)
	}

	fmt.Printf("PART 1: TODO\n")
}

func main() {
	partOne()
}
