package main

import "testing"

var tests = []struct {
	sentence string
	expected Bag
}{
	{
		"light red bags contain 1 bright white bag, 2 muted yellow bags.",
		Bag{"light red", []BagContent{{1, "bright white"}, {2, "muted yellow"}}},
	},
	{
		"dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
		Bag{"dark orange", []BagContent{{3, "bright white"}, {4, "muted yellow"}}},
	},
	{
		"bright white bags contain 1 shiny gold bag.",
		Bag{"bright white", []BagContent{{1, "shiny gold"}}},
	},
	{
		"muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
		Bag{"muted yellow", []BagContent{{2, "shiny gold"}, {9, "faded blue"}}},
	},
	{
		"shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
		Bag{"shiny gold", []BagContent{{1, "dark olive"}, {2, "vibrant plum"}}},
	},
	{
		"dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
		Bag{"dark olive", []BagContent{{3, "faded blue"}, {4, "dotted black"}}},
	},
	{
		"vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
		Bag{"vibrant plum", []BagContent{{5, "faded blue"}, {6, "dotted black"}}},
	},
	{
		"faded blue bags contain no other bags.",
		Bag{"faded blue", []BagContent{}},
	},
	{
		"dotted black bags contain no other bags.",
		Bag{"dotted black", []BagContent{}},
	},
}

func sameContents(contentsOne, contentsTwo []BagContent) bool {
	if len(contentsOne) != len(contentsTwo) {
		return false
	}
	for ind, item := range contentsOne {
		if item != contentsTwo[ind] {
			return false
		}
	}
	return true
}

func sameBag(bagOne, bagTwo Bag) bool {
	sameColor := bagOne.color == bagTwo.color
	return sameColor && sameContents(bagOne.contents, bagTwo.contents)
}

func TestRowCol(t *testing.T) {
	for _, test := range tests {
		parsedBag := parseBag(test.sentence)
		if !sameBag(parsedBag, test.expected) {
			var msg string
			msg += "Bag parsing error.\n"
			msg += "Sentence:  %s\n"
			msg += "Parsed:    %v\n"
			msg += "Expected:  %v\n"
			t.Errorf(msg, test.sentence, parsedBag, test.expected)
		}
	}
}
