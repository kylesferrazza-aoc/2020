{
  description = "Advent of Code 2020 Solutions in Go";

  outputs = { self, nixpkgs }:
  let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      name = "adventofcode2020";
      buildInputs = with pkgs; [
        go
        gopls
      ];
    };
  };
}
